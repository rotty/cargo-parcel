use parcel_tasks::Parcel;

fn main() -> ! {
    let parcel = Parcel::build("cargo-parcel", env!("CARGO_PKG_VERSION"))
        .cargo_binaries(&["cargo-parcel"])
        .finish();
    parcel_tasks::main(parcel);
}
