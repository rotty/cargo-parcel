use anyhow::{anyhow, Context as _};

fn cargo_toml_error(message: impl Into<String>) -> anyhow::Error {
    anyhow!("could not read Cargo.toml: {}", message.into())
}

/// The configuration, as parsed from `Cargo.toml`.
#[derive(Debug)]
pub struct Config {
    name: String,
    version: String,
    metadata: Metadata,
}

impl Config {
    /// Constructs the config from a `Cargo.toml` manifest.
    pub fn from_manifest(manifest: &toml::value::Table) -> anyhow::Result<Self> {
        let malformed_package = |e| anyhow!("malformed package table: {}", e);
        if let Some(parcel) = get_path(manifest, &["workspace", "metadata", "parcel"], toml_table)?
        {
            let metadata = Metadata::from_toml(parcel)
                .map_err(|e| anyhow!("malformed workspace parcel metadata: {e}"))?;
            let name = metadata
                .name
                .as_ref()
                .ok_or_else(|| anyhow!("missing `name` in workspace parcel metadata"))?
                .to_owned();
            let version = metadata
                .version
                .as_ref()
                .ok_or_else(|| anyhow!("missing `version` in workspace parcel metadata"))?
                .to_owned();
            Ok(Config {
                name,
                version,
                metadata,
            })
        } else {
            let package = get_key(manifest, "package", toml_table)?;
            let package_name = get_key(package, "name", toml_str).map_err(malformed_package)?;
            let package_version =
                get_key(package, "version", toml_str).map_err(malformed_package)?;
            let metadata =
                if let Some(parcel) = get_path(package, &["metadata", "parcel"], toml_table)? {
                    Metadata::from_toml(parcel)
                        .map_err(|e| anyhow!("malformed parcel metadata: {}", e))?
                } else {
                    Default::default()
                };
            Ok(Config {
                name: package_name.into(),
                version: package_version.into(),
                metadata,
            })
        }
    }

    /// Returns the package name.
    ///
    /// This will return the `pkg-name` metadata entry. If that key is missing,
    /// the crate's name is returned.
    pub fn package_name(&self) -> &str {
        self.metadata.name.as_deref().unwrap_or(&self.name)
    }

    /// Returns the package version string.
    pub fn package_version(&self) -> &str {
        self.metadata.version.as_deref().unwrap_or(&self.version)
    }

    /// Returns the cargo binaries.
    pub fn cargo_binaries(&self) -> impl Iterator<Item = &str> {
        self.metadata.cargo_binaries.iter().map(|s| s.as_ref())
    }

    /// Returns the man pages.
    pub fn man_pages(&self) -> impl Iterator<Item = &str> {
        self.metadata.man_pages.iter().map(|s| s.as_ref())
    }

    /// Returns the package data resources.
    pub fn pkg_data(&self) -> impl Iterator<Item = &str> {
        self.metadata.pkg_data.iter().map(|s| s.as_ref())
    }

    /// Returns the systemd units.
    pub fn systemd_units(&self) -> impl Iterator<Item = &str> {
        self.metadata.systemd_units.iter().map(|s| s.as_ref())
    }
}

fn toml_str_array(value: &toml::Value) -> anyhow::Result<Vec<String>> {
    let array = value
        .as_array()
        .ok_or_else(|| anyhow!("must be an array"))?;
    array
        .iter()
        .map(|item| {
            item.as_str()
                .map(ToOwned::to_owned)
                .ok_or_else(|| anyhow!("contains non-string item '{}'", item))
        })
        .collect()
}

#[derive(Debug, Default)]
struct Metadata {
    name: Option<String>,
    version: Option<String>,
    cargo_binaries: Vec<String>,
    man_pages: Vec<String>,
    pkg_data: Vec<String>,
    systemd_units: Vec<String>,
}

impl Metadata {
    fn from_toml(metadata: &toml::value::Table) -> anyhow::Result<Self> {
        Ok(Metadata {
            name: get_key_opt(metadata, "name", toml_str)?.map(|s| s.to_owned()),
            version: get_key_opt(metadata, "version", toml_str)?.map(|s| s.to_owned()),
            cargo_binaries: get_key_opt(metadata, "cargo-binaries", toml_str_array)?
                .unwrap_or_default(),
            man_pages: get_key_opt(metadata, "man-pages", toml_str_array)?.unwrap_or_default(),
            pkg_data: get_key_opt(metadata, "pkg-data", toml_str_array)?.unwrap_or_default(),
            systemd_units: get_key_opt(metadata, "systemd-units", toml_str_array)?
                .unwrap_or_default(),
        })
    }
}

fn toml_table(value: &toml::Value) -> anyhow::Result<&toml::value::Table> {
    value.as_table().ok_or_else(|| anyhow!("must be a table"))
}

fn toml_str(value: &toml::Value) -> anyhow::Result<&str> {
    value.as_str().ok_or_else(|| anyhow!("must be a string"))
}

fn get_key<'a, F, T>(table: &'a toml::value::Table, name: &str, f: F) -> anyhow::Result<T>
where
    F: FnOnce(&'a toml::Value) -> anyhow::Result<T>,
{
    get_key_opt(table, name, f)?
        .ok_or_else(|| cargo_toml_error(format!("required key '{name}' missing")))
}

fn get_key_opt<'a, F, T>(
    table: &'a toml::value::Table,
    name: &str,
    f: F,
) -> anyhow::Result<Option<T>>
where
    F: FnOnce(&'a toml::Value) -> anyhow::Result<T>,
{
    table
        .get(name)
        .map(|v| f(v).map_err(|e| anyhow!("invalid value for key '{}': {}", name, e)))
        .transpose()
}

fn get_path<'a, F, T>(
    table: &'a toml::value::Table,
    path: &[&str],
    f: F,
) -> anyhow::Result<Option<T>>
where
    F: FnOnce(&'a toml::Value) -> anyhow::Result<T>,
{
    let mut table = table;
    let (last, steps) = path.split_last().expect("non-empty path");
    for step in steps {
        match table.get(*step) {
            Some(toml::Value::Table(container)) => {
                table = container;
            }
            Some(_) => return Err(anyhow!("invalid value for key '{}', expected table", *step)),
            None => return Ok(None),
        }
    }
    table
        .get(*last)
        .map(|v| f(v).with_context(|| format!("invalid value for key '{}'", path.join("."))))
        .transpose()
}

#[cfg(test)]
mod tests {
    use super::*;
    use toml::toml;

    #[test]
    fn package_manifest() {
        let manifest = toml! {
            [package]
            name = "foo"
            version = "0.42.23"
        };
        let config = Config::from_manifest(&manifest).unwrap();
        assert_eq!(config.package_name(), "foo");
        assert_eq!(config.package_version(), "0.42.23");
    }

    #[test]
    fn package_manifest_overrides() {
        let manifest = toml! {
            [package]
            name = "foo"
            version = "0.42.23"

            [package.metadata.parcel]
            name = "foo-bar"
            version = "1.2.3"
        };
        let config = Config::from_manifest(&manifest).unwrap();
        assert_eq!(config.package_name(), "foo-bar");
        assert_eq!(config.package_version(), "1.2.3");
    }

    #[test]
    fn workspace_manifest() {
        let manifest = toml! {
            [workspace.metadata.parcel]
            name = "foo-bar"
            version = "1.2.3"
        };
        let config = Config::from_manifest(&manifest).unwrap();
        assert_eq!(config.package_name(), "foo-bar");
        assert_eq!(config.package_version(), "1.2.3");
    }
}
