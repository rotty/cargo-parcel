use std::{
    fs,
    io::{self, Write as _},
    path::Path,
};

use anyhow::{bail, Context as _};
use parcel_tasks::{util::EnvError, Command, LoadConfig};
use pico_args::Arguments;

pub struct InitCommand;

impl Command for InitCommand {
    fn name(&self) -> &str {
        "init"
    }

    fn description(&self) -> &str {
        "Add xtask crate based on parcel-tasks."
    }

    fn help(&self) -> &str {
        INIT_HELP
    }

    fn run(
        &self,
        mut args: Arguments,
        _load_config: LoadConfig,
    ) -> Result<(), parcel_tasks::Error> {
        let opts = Options::from_args(&mut args)?;
        let remainder = args.finish();
        if !remainder.is_empty() {
            return Err(EnvError::ExtraneousArgs(remainder).into());
        }
        write_xtask_crate(&opts)?;
        write_cargo_alias()?;
        Ok(())
    }
}

#[derive(Debug)]
struct Options {
    parcel_name: String,
    binaries: Vec<String>,
}

impl Options {
    fn from_args(args: &mut Arguments) -> anyhow::Result<Self> {
        let parcel_name = args.free_from_str()?;
        let binaries = args.values_from_str("--bin")?;
        Ok(Self {
            parcel_name,
            binaries,
        })
    }
}

fn write_cargo_alias() -> anyhow::Result<()> {
    let config_path = ".cargo/config.toml";
    match fs::metadata(config_path) {
        Ok(_) => {
            bail!("'{config_path}' exists, not modifying");
        }
        Err(e) if e.kind() == io::ErrorKind::NotFound => {
            fs::create_dir_all(".cargo")?;
            fs::write(config_path, CARGO_CONFIG_CONTENTS)
                .with_context(|| format!("Could not write '{config_path}'"))?;
        }
        Err(e) => {
            bail!("Could not check for existence of '{config_path}': {e}");
        }
    }
    Ok(())
}

fn write_xtask_crate(opts: &Options) -> anyhow::Result<()> {
    match fs::metadata("xtask") {
        Ok(_) => {
            // TODO: could check file type for better error here.
            bail!("`xtask` exists, not creating");
        }
        Err(e) if e.kind() == io::ErrorKind::NotFound => {
            for dir in &["xtask", "xtask/src"] {
                fs::create_dir(dir)?;
            }
            for (filename, contents) in &[
                ("Cargo.toml", XTASK_CARGO_TOML_CONTENTS),
                (".gitignore", XTASK_GITIGNORE_CONTENTS),
            ] {
                let path = Path::new("xtask").join(filename);
                fs::write(path, contents)?;
            }
            let main_rs_path = Path::new("xtask/src/main.rs");
            write_main_rs(main_rs_path, opts)
                .with_context(|| format!("could not write '{}'", main_rs_path.display()))?;
        }
        Err(e) => {
            bail!("Could not check for existence of `xtask`: {}", e);
        }
    }
    Ok(())
}

fn write_main_rs(path: &Path, opts: &Options) -> io::Result<()> {
    let main_rs = fs::File::create_new(path)?;
    let mut output = io::BufWriter::new(main_rs);

    let binaries = if opts.binaries.is_empty() {
        vec![opts.parcel_name.as_str()]
    } else {
        opts.binaries.iter().map(String::as_str).collect()
    };
    let binary_names: Vec<_> = binaries.iter().map(|name| format!("\"{name}\"")).collect();
    writeln!(output, "use parcel_tasks::Parcel;")?;
    writeln!(output)?;
    writeln!(output, "fn main() -> ! {{")?;
    writeln!(
        output,
        r#"    let parcel = Parcel::build("{}", env!("CARGO_PKG_VERSION"))"#,
        opts.parcel_name,
    )?;
    writeln!(
        output,
        "        .cargo_binaries(&[{}])",
        binary_names.join(", ")
    )?;
    writeln!(output, "        .finish();")?;
    writeln!(output, "    parcel_tasks::main(parcel);")?;
    writeln!(output, "}}")?;
    Ok(())
}

static CARGO_CONFIG_CONTENTS: &str = r#"[alias]
xtask = "run --manifest-path ./xtask/Cargo.toml --"
"#;

static XTASK_GITIGNORE_CONTENTS: &str = r#"/target
"#;

static XTASK_CARGO_TOML_CONTENTS: &str = concat!(
    r#"[package]
name = "xtask"
version = "0.0.1"
edition = "2021"
publish = false

[dependencies]
anyhow = "1.0"
parcel-tasks = ""#,
    env!("CARGO_PKG_VERSION"),
    r#""
"#
);

static INIT_HELP: &str = r#"Add parcel xtask crate

USAGE:

    cargo parcel init

Prepare a package for use with `cargo-parcel`.
"#;
