#![deny(unsafe_code)]
#![warn(rust_2018_idioms)]

use std::{fmt, fs, io, path::PathBuf};

use cargo_parcel::{config::Config, init::InitCommand};
use pico_args::Arguments;

use parcel_tasks::{util::EnvError, CommandSet, ManPageError, Parcel, ResolveError};

#[derive(Debug)]
enum ConfigError {
    Read(io::Error),
    TomlParse(toml::de::Error),
    InvalidManifest(anyhow::Error),
    ManPage(ManPageError),
    Resolve(ResolveError),
}

#[derive(Debug)]
struct ConfigFileError {
    file: PathBuf,
    error: ConfigError,
}

impl fmt::Display for ConfigFileError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.error {
            ConfigError::Read(error) => {
                write!(f, "could not read '{}': {error}", self.file.display())
            }
            ConfigError::TomlParse(error) => write!(
                f,
                "could not parse TOML from '{}': {error}",
                self.file.display()
            ),
            ConfigError::InvalidManifest(error) => write!(
                f,
                "invalid TOML manifest in '{}': {error}",
                self.file.display()
            ),
            ConfigError::ManPage(error) => write!(f, "{error}"),
            ConfigError::Resolve(error) => write!(f, "{error}"),
        }
    }
}

impl std::error::Error for ConfigFileError {}

/// Load the configuration from `Cargo.toml`.
pub fn load_config(_args: &mut Arguments) -> anyhow::Result<Parcel> {
    let config_path = PathBuf::from("Cargo.toml");
    let make_error = |inner| ConfigFileError {
        file: config_path.clone(),
        error: inner,
    };
    let manifest_contents =
        fs::read_to_string(&config_path).map_err(|error| make_error(ConfigError::Read(error)))?;
    let manifest: toml::value::Table = toml::from_str(&manifest_contents)
        .map_err(|error| make_error(ConfigError::TomlParse(error)))?;
    let config = Config::from_manifest(&manifest)
        .map_err(|error| make_error(ConfigError::InvalidManifest(error)))?;
    let parcel = Parcel::build(config.package_name(), config.package_version())
        .cargo_binaries(config.cargo_binaries())
        .man_pages(config.man_pages())
        .map_err(|error| make_error(ConfigError::ManPage(error)))?
        .pkg_data(config.pkg_data())
        .map_err(|error| make_error(ConfigError::Resolve(error)))?
        .systemd_units(config.systemd_units())
        .map_err(|error| make_error(ConfigError::Resolve(error)))?
        .finish();
    Ok(parcel)
}

fn run() -> anyhow::Result<()> {
    let mut commands = CommandSet::builtin();
    commands.push(InitCommand);
    let is_cargo_plugin = std::env::args_os()
        .nth(1)
        .map_or(false, |argv1| argv1.to_str() == Some("parcel"));
    let first_arg = if is_cargo_plugin { 2 } else { 1 };
    let subcommand = match std::env::args_os().nth(first_arg) {
        None => {
            commands.help_summary();
            return Ok(());
        }
        Some(s) => s,
    };
    let Some(subcommand) = subcommand.to_str() else {
        return Err(EnvError::Args(pico_args::Error::NonUtf8Argument).into());
    };
    let args = Arguments::from_vec(std::env::args_os().skip(first_arg + 1).collect());
    commands.dispatch(subcommand, args, load_config)?;
    Ok(())
}

pub fn main() -> ! {
    let rc = match run() {
        Ok(()) => 0,
        Err(e) => {
            eprintln!("cargo-parcel: {e}");
            let mut e: &dyn std::error::Error = e.as_ref();
            while let Some(source) = e.source() {
                eprintln!("    caused by: {}", source);
                e = source;
            }
            1
        }
    };
    std::process::exit(rc);
}
