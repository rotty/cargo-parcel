use std::{
    env, fs,
    io::{self, Write},
    path::Path,
};

fn env_lang() -> Option<String> {
    // Kludgey extraction of the country code from the LANG environment
    // variable.
    let lang = env::var_os("LANG").and_then(|l| l.into_string().ok())?;
    let lang = if let Some(dot) = lang.find('.') {
        &lang[..dot]
    } else {
        &lang
    };
    let lang = if let Some(underscore) = lang.find('_') {
        &lang[..underscore]
    } else {
        &lang
    };
    Some(lang.into())
}

fn main() {
    match (env_lang(), option_env!("PARCEL_INSTALL_PREFIX")) {
        (Some(lang), Some(prefix)) => {
            let path =
                Path::new(prefix).join(format!("share/hello-world-i18n/message/{}.txt", lang));
            let message = fs::read(&path).unwrap_or_else(|e| {
                panic!("could not read message file {}: {}", path.display(), e)
            });
            let mut stdout = io::stdout();
            stdout.write(&message).unwrap();
        }
        _ => {
            println!("Hello, world!");
        }
    }
}
