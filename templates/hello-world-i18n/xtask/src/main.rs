use parcel_tasks::Parcel;

fn main() -> anyhow::Result<()> {
    let parcel = Parcel::build("hello-world-i18n", env!("CARGO_PKG_VERSION"))
        .cargo_binaries(&["hello-world-i18n"])
        .pkg_data(&["data/*"])?
        .finish();
    parcel_tasks::main(parcel);
}
