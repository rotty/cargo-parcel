# cargo-parcel project templates

This directory contains templates that provide minimal examples of
integrating cargo-parcel into a project, and can consist almost
entirely of boilerplate that can be copied verbatim into an
application crate to make it use cargo-parcel.

The `hello-world` template contains a basic integration for a project
that does not need to have its installation prefix compiled in, while
`hello-world-i18n` shows how to make use of the installation prefix.

See the [README](../README.md) for detailed integration instructions;
here is the short version:

- Copy the contents of the `xtask` directory, and
  `.cargo/config.toml` into your crate's root directory.
- Add the `xtask` directory as a workspace member in your crate's
  `Cargo.toml`.
- Add appropriate metadata to your crate's `Cargo.toml` in the
  `package.metadata.parcel` section.
