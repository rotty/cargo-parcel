% HELLO-WORLD(1) hello-world Manual
% Andreas Rottmann
% January, 2020

# NAME

hello-world -  Prints "Hello World!"

# SYNOPSIS

__hello-world__

# DESCRIPTION

This is just a toy example printing the string "Hello World!", without
the quotes, and terminated by a newline.

# EXIT STATUS

- 0 — Successful termination
