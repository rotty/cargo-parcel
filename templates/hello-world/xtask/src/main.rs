use parcel_tasks::Parcel;

fn main() -> anyhow::Result<()> {
    let parcel = Parcel::build("hello-world", env!("CARGO_PKG_VERSION"))
        .cargo_binaries(&["hello-world"])
        .man_pages(&["hello-world.1.md"])?
        .finish();
    parcel_tasks::main(parcel);
}
