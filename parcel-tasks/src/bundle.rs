use std::{
    ffi::OsString,
    path::{Path, PathBuf},
    str::FromStr,
};

use pico_args::Arguments;
use tempfile::TempDir;

use crate::{
    install::InstallOpt,
    util::{self, empty_env, EnvError},
    Error, Parcel,
};

#[derive(Debug, Clone)]
pub struct Options {
    prefix_skip: usize,
    prefix: PathBuf,
    root: PathBuf,
    config: Parcel,
    strip: bool,
    verbose: bool,
    target: Option<String>,
    default_features: bool,
    features: Vec<String>,
    output: Output,
    tar_command: String,
}

#[derive(Debug, Clone)]
enum Output {
    Path(String),
    Stdout,
}

impl FromStr for Output {
    type Err = std::convert::Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s == "-" {
            Ok(Output::Stdout)
        } else {
            Ok(Output::Path(s.into()))
        }
    }
}

impl Options {
    pub fn from_args(mut args: Arguments, config: &Parcel) -> Result<Self, EnvError> {
        let prefix = util::get_prefix_opt(&mut args)?;
        let opt = Options {
            prefix_skip: prefix.components().count(),
            prefix,
            root: args.opt_value_from_str("--root")?.unwrap_or_else(|| {
                format!("{}-{}", config.pkg_name(), config.pkg_version()).into()
            }),
            config: config.clone(),
            strip: !args.contains("--no-strip"),
            verbose: args.contains("--verbose"),
            target: args.opt_value_from_str("--target")?,
            default_features: !args.contains("--no-default-features"),
            features: util::opt_values(&mut args, "--features")?,
            output: args.opt_value_from_str("-o")?.unwrap_or_else(|| {
                Output::Path(format!(
                    "{}-{}.tar.gz",
                    config.pkg_name(),
                    config.pkg_version()
                ))
            }),
            tar_command: args
                .opt_value_from_str("--tar")?
                .unwrap_or_else(|| "tar".to_owned()),
        };
        let remainder = args.finish();
        if !remainder.is_empty() {
            return Err(EnvError::ExtraneousArgs(remainder));
        }
        Ok(opt)
    }
}

static TAR_COMPRESSION_FLAGS: &[(&str, Option<&str>)] = &[
    (".tar", None),
    (".tar.gz", Some("-z")),
    // The `-j` option is supported by both GNU and BSD tar.
    (".tar.bz2", Some("-j")),
    // The "--xz" option is a GNU extension.
    (".tar.xz", Some("--xz")),
    // As is `--zstd`.
    (".tar.zstd", Some("--zstd")),
];

pub fn create(opt: &Options) -> Result<(), Error> {
    let tmp_dir = TempDir::new().map_err(Error::temp_dir)?;
    InstallOpt::new(opt.config.clone(), &opt.prefix)
        .dest_dir(tmp_dir.path())
        .strip_binaries(opt.strip)
        .cargo_target(opt.target.as_deref())
        .cargo_default_features(opt.default_features)
        .cargo_features(&opt.features)
        .verbose(opt.verbose)
        .install()
        .map_err(Error::install_step)?;
    use std::path::Component;
    let skip_prefix: PathBuf = opt
        .prefix
        .components()
        .skip_while(|c| !matches!(c, Component::Normal(_)))
        .take(opt.prefix_skip)
        .collect();
    let mut transform = OsString::new();
    // TODO: need to check for that `opt.root` is relative when validating the
    // options.
    transform.push("--transform=s,^.,./");
    // FIXME: escape commas
    transform.push(&opt.root);
    transform.push(",");
    let out_args = match &opt.output {
        Output::Path(o) => {
            let mut out_args = vec!["-f", o];
            if let Some(name) = Path::new(o).file_name().and_then(|f| f.to_str()) {
                let compression_flag = TAR_COMPRESSION_FLAGS
                    .iter()
                    .find_map(|(suffix, flag)| if name.ends_with(suffix) { Some(flag) } else { None })
                    .unwrap_or_else(|| {
                        eprintln!("warning: output file with unknown extension, creating an uncompressed tar archive");
                        &None
                    });
                out_args.extend(compression_flag.iter());
            } else {
                eprintln!(
                    "warning: not looking at extension in non-UTF8 output file name {}",
                    o
                );
            }
            out_args
        }
        Output::Stdout => vec![],
    };
    let tar_args = {
        let mut args = vec![
            "-C".into(),
            tmp_dir.path().join(skip_prefix).into(),
            "-c".into(),
            ".".into(),
            transform,
        ];
        args.extend(out_args.iter().map(OsString::from));
        args
    };
    util::cmd(&opt.tar_command, tar_args, empty_env(), opt.verbose)?;
    Ok(())
}
