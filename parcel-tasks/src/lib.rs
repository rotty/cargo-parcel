//! A library implementing an extended version of `cargo install`.
//!
//! The intended use of this crate is in an
//! [`xtask`](https://github.com/matklad/cargo-xtask)-style helper crate,
//! extending `cargo` with a new subcommand, providing a superset of the
//! functionality provided by `cargo install`. For such typical use, a single
//! call to the [`main`] function is sufficient to implement the helper crate's
//! main (and only) program.
//!
//! The rest of the API is provided for experimentation with alternative
//! interfaces to the functionality provided, but should be considered in flux;
//! no particular considerations to backward compatibility will be made,
//! although breaking API changes will result in a semver bump, following the
//! usual Rust conventions.

#![deny(missing_docs, unsafe_code)]
#![warn(rust_2018_idioms)]

use std::{
    fmt, io,
    path::{Path, PathBuf},
};

use pico_args::Arguments;

mod bundle;

mod install;

pub mod util;
use util::{EnvError, ExecError};

mod man;
pub use man::{ManSource, ManSourceFormat, ParseManSourceError};

mod commands;
pub use commands::CommandSet;

mod subst;

/// A description of a parcel's metadata.
#[derive(Debug, Clone)]
pub struct Parcel {
    pkg_name: String,
    pkg_version: String,
    man_pages: Vec<ManSource>,
    pkg_data: Vec<PathBuf>,
    cargo_binaries: Vec<PathBuf>,
    systemd_units: Vec<PathBuf>,
}

/// A CLI subcommand.
pub trait Command: Send + Sync {
    /// The name of the subcommand.
    fn name(&self) -> &str;
    /// The description of the subcommand.
    ///
    /// May be multiple (short) sentences and should end with a period.
    fn description(&self) -> &str;
    /// The full help text for the subcommand.
    fn help(&self) -> &str;
    /// Run the actual subcommand logic.
    fn run(&self, args: Arguments, load_config: LoadConfig) -> Result<(), Error>;
}

/// Callback to obtain a config.
pub type LoadConfig = Box<dyn FnOnce(&mut pico_args::Arguments) -> Result<Parcel, anyhow::Error>>;

/// Describes an error caused by an invalid man page specification.
#[derive(Debug)]
pub struct ManPageError {
    source: String,
    error: anyhow::Error,
}

impl fmt::Display for ManPageError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "malformed man page source file name '{}': {}",
            self.source, self.error
        )
    }
}

impl std::error::Error for ManPageError {}

/// Describes an error caused by a invalid glob item.
#[derive(Debug)]
pub struct ResolveError {
    kind: &'static str,
    item: String,
    error: anyhow::Error,
}

impl fmt::Display for ResolveError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "could not resolve {} item '{}': {}",
            self.kind, self.item, self.error
        )
    }
}

impl std::error::Error for ResolveError {}

/// A builder allowing to configuration of optional parcel properties.
// We can re-use `Parcel` here; this works since we always maintain a valid
// state.
#[derive(Debug)]
pub struct ParcelBuilder(Parcel);

impl ParcelBuilder {
    /// Configures the cargo binaries.
    pub fn cargo_binaries<I>(mut self, bins: I) -> Self
    where
        I: IntoIterator,
        I::Item: AsRef<str>,
    {
        for bin in bins {
            self.0.cargo_binaries.push(bin.as_ref().into())
        }
        self
    }

    /// Configures the man pages to install.
    pub fn man_pages<I>(mut self, sources: I) -> Result<Self, ManPageError>
    where
        I: IntoIterator,
        I::Item: AsRef<str>,
    {
        for source in sources {
            let source = source.as_ref();
            let parsed = source.parse::<ManSource>().map_err(|e| ManPageError {
                source: source.to_owned(),
                error: e.into(),
            })?;
            self.0.man_pages.push(parsed);
        }
        Ok(self)
    }

    /// Configures data files and directories to be installed in an
    /// application-specific directory.
    ///
    /// The strings passed will be expanded as glob expressions, and need to
    /// refer to existing files or directories, relative to the crate root
    /// directory. The resulting list of paths will be copied, recursively, in
    /// the case of directories into the application-specific data directory
    /// below the installation prefix. Note they will be placed in that
    /// directory using their file name, ignoring any leading directory
    /// components.
    pub fn pkg_data<I>(mut self, pkg_data: I) -> Result<Self, ResolveError>
    where
        I: IntoIterator,
        I::Item: AsRef<str>,
    {
        resolve_globs("package data", pkg_data, &mut self.0.pkg_data)?;
        Ok(self)
    }

    /// Configures systemd unit files to be installed.
    ///
    /// Similar to the `pkg_data` method, the passed strings are
    /// expanded as glob expressions.
    pub fn systemd_units<I>(mut self, units: I) -> Result<Self, ResolveError>
    where
        I: IntoIterator,
        I::Item: AsRef<str>,
    {
        resolve_globs("systemd unit", units, &mut self.0.systemd_units)?;
        Ok(self)
    }

    /// Finishes configuration and returns the parcel as configured.
    pub fn finish(self) -> Parcel {
        self.0
    }
}

fn resolve_globs<I>(
    kind: &'static str,
    patterns: I,
    resolved: &mut Vec<PathBuf>,
) -> Result<(), ResolveError>
where
    I: IntoIterator,
    I::Item: AsRef<str>,
{
    for pattern in patterns {
        let pattern = pattern.as_ref();
        let paths = glob::glob_with(
            pattern,
            glob::MatchOptions {
                case_sensitive: true,
                require_literal_separator: true,
                require_literal_leading_dot: true,
            },
        )
        .map_err(|e| ResolveError {
            kind: "package data",
            item: pattern.to_owned(),
            error: e.into(),
        })?;
        for path in paths {
            let path = path.map_err(|e| ResolveError {
                kind,
                item: pattern.to_owned(),
                error: e.into(),
            })?;
            resolved.push(path);
        }
    }
    Ok(())
}

impl Parcel {
    /// Create an empty parcel.
    ///
    /// The given name and version strings will be used to determine
    /// installation paths for resources.
    pub fn build<T: Into<String>, U: Into<String>>(pkg_name: T, version: U) -> ParcelBuilder {
        ParcelBuilder(Parcel {
            pkg_name: pkg_name.into(),
            pkg_version: version.into(),
            man_pages: Vec::new(),
            pkg_data: Vec::new(),
            cargo_binaries: Vec::new(),
            systemd_units: Vec::new(),
        })
    }

    /// Returns the package name.
    pub fn pkg_name(&self) -> &str {
        &self.pkg_name
    }

    /// Returns the package version string.
    pub fn pkg_version(&self) -> &str {
        &self.pkg_version
    }

    /// Returns the man pages to be installed.
    pub fn pkg_data(&self) -> impl Iterator<Item = &Path> {
        self.pkg_data.iter().map(|s| s.as_ref())
    }

    /// Returns the cargo binaries to be installed.
    pub fn cargo_binaries(&self) -> impl Iterator<Item = &Path> {
        self.cargo_binaries.iter().map(|s| s.as_ref())
    }

    /// Returns the man pages to be installed.
    pub fn man_pages(&self) -> impl Iterator<Item = &ManSource> {
        self.man_pages.iter()
    }
}

fn run(load_config: LoadConfig) -> Result<(), Error> {
    let commands = CommandSet::builtin();
    let subcommand = match std::env::args_os().nth(1) {
        None => {
            commands.help_summary();
            return Ok(());
        }
        Some(s) => s,
    };
    let Some(subcommand) = subcommand.to_str() else {
        return Err(EnvError::Args(pico_args::Error::NonUtf8Argument).into());
    };
    let args = Arguments::from_vec(std::env::args_os().skip(2).collect());
    commands.dispatch(subcommand, args, load_config)?;
    Ok(())
}

/// Run the command line interface, with a dynamically computed configuration.
///
/// In contrast to [`main`], the configuration is computed by the
/// provided function, which may consume arguments from the command
/// line.
pub fn main_with<F>(load_config: F) -> !
where
    F: FnOnce(&mut pico_args::Arguments) -> anyhow::Result<Parcel> + 'static,
{
    let rc = match run(Box::new(load_config)) {
        Ok(()) => 0,
        Err(error) => {
            eprintln!("cargo-parcel: {error}");
            let mut e = &error as &dyn std::error::Error;
            while let Some(source) = e.source() {
                eprintln!("    caused by: {source}");
                e = source;
            }
            1
        }
    };
    std::process::exit(rc);
}

/// Run the command line interface.
///
/// This is the top-level entry point to the library's functionality, and the
/// only function needed inside a typical helper crate when using cargo-parcel
/// to provide an extended version of `cargo install` for a project.
pub fn main(parcel: Parcel) -> ! {
    main_with(|_args| Ok(parcel))
}

/// Encompasses all possible errors of the crate.
#[derive(Debug)]
pub struct Error(ErrorRepr);

impl Error {
    fn unknown_subcommand(name: impl Into<String>) -> Self {
        Self(ErrorRepr::UnknownSubcommand(name.into()))
    }

    fn unknown_help_subcommand(name: impl Into<String>) -> Self {
        Self(ErrorRepr::UnknownHelpSubcommand(name.into()))
    }

    fn install_step(inner: anyhow::Error) -> Self {
        Self(ErrorRepr::InstallStep(inner))
    }

    fn uninstall_step(inner: anyhow::Error) -> Self {
        Self(ErrorRepr::UninstallStep(inner))
    }

    fn temp_dir(inner: io::Error) -> Self {
        Self(ErrorRepr::TempDir(inner))
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.0 {
            ErrorRepr::Env(inner) => write!(f, "{inner}"),
            ErrorRepr::Exec(inner) => write!(f, "{inner}"),
            ErrorRepr::Custom(inner) => write!(f, "{inner}"),
            ErrorRepr::UnknownSubcommand(name) => write!(f, "unknown subcommand '{name}'"),
            ErrorRepr::UnknownHelpSubcommand(name) => {
                write!(f, "help requested for unknown subcommand '{name}'")
            }
            ErrorRepr::InstallStep(error) => {
                write!(f, "error during install step: {error}")
            }
            ErrorRepr::UninstallStep(error) => {
                write!(f, "error during uninstall step: {error}")
            }
            ErrorRepr::TempDir(error) => write!(f, "could not crate temporary directory: {error}"),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match &self.0 {
            ErrorRepr::InstallStep(error) => error.source(),
            ErrorRepr::UninstallStep(error) => error.source(),
            ErrorRepr::Custom(error) => error.source(),
            _ => None,
        }
    }
}

impl From<EnvError> for Error {
    fn from(value: EnvError) -> Self {
        Error(ErrorRepr::Env(value))
    }
}

impl From<ExecError> for Error {
    fn from(value: ExecError) -> Self {
        Error(ErrorRepr::Exec(value))
    }
}

impl From<anyhow::Error> for Error {
    fn from(value: anyhow::Error) -> Self {
        Error(ErrorRepr::Custom(value))
    }
}

#[derive(Debug)]
enum ErrorRepr {
    Env(EnvError),
    Exec(ExecError),
    TempDir(io::Error),
    InstallStep(anyhow::Error),
    UninstallStep(anyhow::Error),
    UnknownSubcommand(String),
    UnknownHelpSubcommand(String),
    Custom(anyhow::Error),
}
