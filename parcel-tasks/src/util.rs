//! Miscellaneous utilities

use std::{
    ffi::{OsStr, OsString},
    fmt, io,
    path::PathBuf,
    process::{Command, ExitStatus},
};

use pico_args::Arguments;

/// Encompasses errors possible during execution of external commands.
#[derive(Debug)]
pub struct ExecError {
    program: OsString,
    args: Vec<OsString>,
    reason: ExecErrorReason,
}

impl fmt::Display for ExecError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.reason {
            ExecErrorReason::Status(status) => {
                write!(
                    f,
                    "subprocess '{} {}' failed with status {}",
                    self.program.to_string_lossy(),
                    DisplayArgs(self.args.iter()),
                    status
                )
            }
            ExecErrorReason::Io(error) => {
                write!(
                    f,
                    "could not run '{} {}': {}",
                    self.program.to_string_lossy(),
                    DisplayArgs(self.args.iter()),
                    error
                )
            }
        }
    }
}

impl std::error::Error for ExecError {}

#[derive(Debug)]
enum ExecErrorReason {
    Io(io::Error),
    Status(ExitStatus),
}

/// Encompasses errors found in the program's environment.
#[derive(Debug)]
pub enum EnvError {
    /// Command-line argument parsing error.
    Args(pico_args::Error),
    /// Extraneous arguments left over after command-line argument parsing.
    ExtraneousArgs(Vec<OsString>),
    /// An error related to an environment variable.
    Var {
        /// Name of the environment variable.
        name: String,
        /// Error encountered with the environment variable.
        error: std::env::VarError,
    },
}

impl From<pico_args::Error> for EnvError {
    fn from(value: pico_args::Error) -> Self {
        EnvError::Args(value)
    }
}

impl fmt::Display for EnvError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            EnvError::Args(error) => write!(f, "{error}"),
            EnvError::ExtraneousArgs(args) => {
                write!(
                    f,
                    "extraneous arguments found: {}",
                    DisplayArgs(args.iter())
                )
            }
            EnvError::Var { name, error } => write!(f, "environment variable '{name}': {error}"),
        }
    }
}

impl std::error::Error for EnvError {}

/// Display a list of command-line arguments.
#[derive(Debug)]
pub struct DisplayArgs<T>(pub T);

impl<T> fmt::Display for DisplayArgs<T>
where
    T: Iterator + Clone,
    T::Item: AsRef<OsStr>,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut args = self.0.clone();
        let mut next = args.next();
        while let Some(arg) = next {
            // FIXME: quoting
            let s = arg.as_ref().to_string_lossy();
            write!(f, "{}", s)?;
            next = args.next();
            if next.is_some() {
                write!(f, " ")?;
            }
        }
        Ok(())
    }
}

/// Construct an empty set of environment variables.
pub fn empty_env() -> Vec<(&'static str, &'static OsStr)> {
    Vec::new()
}

/// Run a program in a subprocess.
///
/// Blockingly runs the given executable with the supplied arguments and
/// environment.
pub fn cmd<P, E, A, V>(program: P, args: A, env: E, verbose: bool) -> Result<(), ExecError>
where
    P: AsRef<OsStr>,
    A: IntoIterator,
    A::IntoIter: Clone,
    A::Item: AsRef<OsStr>,
    E: IntoIterator<Item = (&'static str, V)>,
    V: AsRef<OsStr>,
{
    let program = program.as_ref();
    let args = args.into_iter();
    let env = env.into_iter();
    if verbose {
        println!(
            "* {} {}",
            program.to_string_lossy(),
            DisplayArgs(args.clone())
        );
    }
    let make_error = |reason| ExecError {
        program: program.to_owned(),
        args: args.clone().map(|arg| arg.as_ref().to_owned()).collect(),
        reason,
    };
    let status = Command::new(program)
        .args(args.clone())
        .envs(env)
        .status()
        .map_err(|e| make_error(ExecErrorReason::Io(e)))?;
    if !status.success() {
        return Err(ExecError {
            program: program.to_owned(),
            args: args.map(|arg| arg.as_ref().to_owned()).collect(),
            reason: ExecErrorReason::Status(status),
        });
    }
    Ok(())
}

/// Get the path prefix to use.
///
/// If present in the supplied arguments, return the value given for
/// `--prefix`. Otherwise, it will return a default, currently `~/.local` for
/// all platforms.
pub fn get_prefix_opt(args: &mut Arguments) -> Result<PathBuf, EnvError> {
    match args
        .opt_value_from_str("--prefix")
        .map_err(EnvError::Args)?
    {
        Some(prefix) => Ok(prefix),
        None => {
            let mut home = PathBuf::from(std::env::var("HOME").map_err(|error| EnvError::Var {
                name: "HOME".into(),
                error,
            })?);
            home.push(".local");
            Ok(home)
        }
    }
}

/// Collect values for an option.
///
/// Consumes and returns all arguments with the given option
/// name. Individual argument values are split on space and comma
/// before being inserted into the returned vector.
pub fn opt_values(
    args: &mut Arguments,
    name: &'static str,
) -> Result<Vec<String>, pico_args::Error> {
    let mut values = Vec::new();
    while let Some(value) = args.opt_value_from_str::<_, String>(name)? {
        values.extend(value.split(&[',', ' ']).map(ToOwned::to_owned));
    }
    Ok(values)
}
