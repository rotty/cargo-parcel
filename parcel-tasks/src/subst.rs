use std::{
    collections::HashMap,
    io::{self, BufRead as _},
};

pub fn substitute<R, W>(
    reader: &mut R,
    writer: &mut W,
    table: &HashMap<&str, &str>,
) -> io::Result<()>
where
    R: io::Read + ?Sized,
    W: io::Write + ?Sized,
{
    let reader = io::BufReader::new(reader);
    let mut output = String::new();
    for line in reader.lines() {
        let line = line?;
        output.clear();
        subst(&line, &mut output, table);
        output.push('\n');
        writer.write_all(output.as_bytes())?;
    }
    Ok(())
}

fn subst(input: &str, output: &mut String, table: &HashMap<&str, &str>) {
    let mut remainder = input;
    while let Some(start) = remainder.find('@') {
        let after_at = &remainder[start + 1..];
        if let Some(end) = after_at.find('@') {
            let keyword = &after_at[..end];
            output.push_str(&remainder[..start]);
            if keyword.is_empty() {
                output.push('@');
            } else if let Some(replacement) = table.get(keyword) {
                output.push_str(replacement);
            } else {
                output.push_str(&remainder[start..start + 1 + end + 1]);
            }
            remainder = &after_at[end + 1..];
        } else {
            break;
        }
    }
    output.push_str(remainder);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn no_subst() {
        let mut output = String::new();
        subst("abcdefg", &mut output, &HashMap::new());
        assert_eq!(output, "abcdefg");
    }

    #[test]
    fn simple_subst() {
        let table = [("prefix", "/usr/local")].into_iter().collect();
        let mut output = String::new();
        subst("@prefix@/bin/example", &mut output, &table);
        assert_eq!(output, "/usr/local/bin/example");
    }

    #[test]
    fn escaped() {
        let mut output = String::new();
        subst("double @@ quadruple @@@@", &mut output, &HashMap::new());
        assert_eq!(output, "double @ quadruple @@");
    }

    #[test]
    fn missing() {
        let mut output = String::new();
        subst("test @missing@ test", &mut output, &HashMap::new());
        assert_eq!(output, "test @missing@ test");
    }
}
