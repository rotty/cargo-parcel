use pico_args::Arguments;

use crate::{bundle, install::InstallOpt, util::EnvError, Command, Error, LoadConfig, Parcel};

/// An ordered set of (sub)commands.
pub struct CommandSet(Vec<Box<dyn Command>>);

impl CommandSet {
    /// Returns the built-in set of commmands.
    pub fn builtin() -> Self {
        Self(vec![
            Box::new(Install),
            Box::new(Uninstall),
            Box::new(Bundle),
            Box::new(Report),
        ])
    }

    /// Add a command to the set.
    pub fn push(&mut self, cmd: impl Command + 'static) {
        self.0.push(Box::new(cmd))
    }

    /// Dispatch to the named command.
    pub fn dispatch<F>(
        &self,
        command: &str,
        mut args: Arguments,
        load_config: F,
    ) -> Result<(), Error>
    where
        F: FnOnce(&mut pico_args::Arguments) -> anyhow::Result<Parcel> + 'static,
    {
        if command == "help" {
            let name = args.opt_free_from_str::<String>().map_err(EnvError::Args)?;
            match name {
                Some(name) => {
                    let Some(cmd) = self.0.iter().find(|cmd| cmd.name() == name) else {
                        return Err(Error::unknown_help_subcommand(command));
                    };
                    print!("{}", cmd.help());
                }
                None => {
                    self.help_summary();
                }
            }
            Ok(())
        } else {
            let cmd = self.0.iter().find(|cmd| cmd.name() == command);
            match cmd {
                Some(cmd) => {
                    if args.contains(["-h", "--help"]) {
                        print!("{}", cmd.help());
                    } else {
                        cmd.run(args, Box::new(load_config))?;
                    }
                    Ok(())
                }
                None => Err(Error::unknown_subcommand(command)),
            }
        }
    }

    /// Prints the help summary for the command set.
    pub fn help_summary(&self) {
        println!("Extended cargo installer");
        println!();
        println!("USAGE:");
        println!();
        println!("cargo parcel [SUBCOMMAND] [OPTIONS]");
        println!();
        println!("The available subcommands are:");
        println!();
        for cmd in &self.0 {
            println!("    {:10} {}", cmd.name(), cmd.description());
        }
        println!();
        println!("See 'cargo parcel help <command>' for more information on a specific command.");
    }
}

struct Install;

impl Command for Install {
    fn name(&self) -> &str {
        "install"
    }

    fn description(&self) -> &str {
        "Install the parcel contents. Default prefix is ~/.local."
    }

    fn help(&self) -> &str {
        INSTALL_HELP
    }

    fn run(&self, mut args: pico_args::Arguments, load_config: LoadConfig) -> Result<(), Error> {
        let parcel = load_config(&mut args)?;
        let opt = InstallOpt::from_args(args, &parcel)?;
        opt.install().map_err(Error::install_step)?;
        Ok(())
    }
}

struct Uninstall;

impl Command for Uninstall {
    fn name(&self) -> &str {
        "uninstall"
    }

    fn description(&self) -> &str {
        "Uninstall the parcel contents."
    }

    fn help(&self) -> &str {
        UNINSTALL_HELP
    }

    fn run(&self, mut args: Arguments, load_config: LoadConfig) -> Result<(), Error> {
        let parcel = load_config(&mut args)?;
        let opt = InstallOpt::from_args(args, &parcel)?;
        opt.uninstall().map_err(Error::uninstall_step)?;
        Ok(())
    }
}

struct Bundle;

impl Command for Bundle {
    fn name(&self) -> &str {
        "bundle"
    }

    fn description(&self) -> &str {
        "Create a distribution bundle."
    }

    fn help(&self) -> &str {
        BUNDLE_HELP
    }

    fn run(&self, mut args: Arguments, load_config: LoadConfig) -> Result<(), Error> {
        let parcel = load_config(&mut args)?;
        let opt = bundle::Options::from_args(args, &parcel)?;
        bundle::create(&opt)?;
        Ok(())
    }
}

struct Report;

impl Command for Report {
    fn name(&self) -> &str {
        "report"
    }

    fn description(&self) -> &str {
        "Describe the build steps."
    }

    fn help(&self) -> &str {
        REPORT_HELP
    }

    fn run(&self, mut args: Arguments, load_config: LoadConfig) -> Result<(), Error> {
        let parcel = load_config(&mut args)?;
        let opt = InstallOpt::from_args(args, &parcel)?;
        opt.report();
        Ok(())
    }
}

static INSTALL_HELP: &str = r#"Install a parcel

USAGE:

    cargo parcel install [OPTIONS]

OPTIONS:

    --verbose             Verbose operation.
    --prefix <DIR>        Installation prefix. This defaults to ~/.local.
    --target <TARGET>     Rust target triple to build for.
    --features <FEATURES> Cargo features to enable.
    --no-default-features Do not enable default cargo features.
    --no-strip            Do not strip the binaries.
    --dest-dir <DIR>      Destination directory, will be prepended to the
                          installation prefix.

This will, after compiling the crate in release mode, install the parcel
contents as described by the "package.metadata.parcel" section in
"Cargo.toml". The files will be installed into "<DESTDIR>/<PREFIX>", where
DESTDIR and PREFIX are specified with the "--dest-dir" and "--prefix" arguments,
respectively. If "--dest-dir" is not given, it defaults to the root directory.

PREFIX should correspond to the final installation directory, and may be
compiled into the binaries. DESTDIR can be used to direct the install to a
staging area.
"#;

static UNINSTALL_HELP: &str = r#"Uninstall a parcel

USAGE:

    cargo parcel uninstall [OPTIONS]

OPTIONS:

    --verbose           Verbose operation.
    --prefix <DIR>      Installation prefix. This defaults to ~/.local.
    --dest-dir <DIR>    Destination directory, will be prepended to the
                        installation prefix.

This will uninstall the parcel contents installed by the "install" command. The
"--prefix" and "--dest-dir" arguments should be the same as given to the
"install" invocation that should be counteracted.
"#;

static BUNDLE_HELP: &str = r#"Create a binary distribution bundle

USAGE:

    cargo parcel bundle [OPTIONS]

OPTIONS:

    --verbose             Verbose operation.
    --prefix <DIR>        Installation prefix. Defaults to ~/.local.
    --root <DIR>          Top-level directory of the created archive. Defaults
                          to <NAME>-<VERSION>.
    --target <TARGET>     Rust target triple to build for.
    --features <FEATURES> Cargo features to enable.
    --no-default-features Do not enable default cargo features.
    -o <FILE>             Output file. The following extensions are
                          supported: ".tar", ".tar.gz", ".tar.bz2", ".tar.xz"
                          and ".tar.zstd".
                          Defaults to <NAME>-<VERSION>.tar.gz.
                          If "-", an uncompressed TAR archive will be written
                          to standard output.
    --tar <CMD>           Command to invoke for GNU tar. Defaults to "tar".

Create a binary distribution bundle, either as TAR format archive on standard
output, or, when the "-o" option is given, as an archive with a format based on
the extension of the given file. The "--prefix" option is the same as for the
"install" command.

This command requires GNU tar. If GNU tar is not available as "tar", you can
specify an alternative, such "gtar" on BSD platforms, using the "--tar" option.
"#;

static REPORT_HELP: &str = "Report what would get installed";
