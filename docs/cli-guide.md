# The `cargo-parcel` command-line interface

For a crate that integrates `parcel-tasks` directly, a `cargo xtask`
command will be available, by way of an `cargo` alias. In this case,
substitute `cargo parcel` with `cargo xtask` in the commands mentioned
below. Also note that when `parcel-tasks` is directly integrated, the
configuration is expressed as code, and will not be loaded from the
configuration file.

If the crate does not have `parcel-tasks` integration, you can still
use the `cargo-parcel` cargo plugin.

Crates authors may wish to provide, and instruct you to use this
interface instead of `cargo install` for (probably) one of these
reasons:

- Using `cargo-parcel` allows the crate author to include pre-install
  steps in the installation process, such as rendering Unix man pages
  from Markdown sources. This feature currently requires installation
  of [`pandoc`].
- `cargo parcel install` can not only install binaries, but also other
  files that may need to, or should, go along with the binaries, such
  as man pages or resource files.
- Using `cargo parcel bundle`you can crate a binary distribution,
  allowing easy deployment on a machine that doesn't have a Rust
  toolchain installed.
- The metadata in `Cargo.toml` that drives `cargo-parcel` may also
  make it easier for e.g. Linux distributions to provide native
  packages of the application.

## Background

When designing `cargo-parcel`, an eye has been kept on making it
useful for downstream packaging. However, the results should be
beneficial for direct users as well:

- Predictable installation contents, given the `Cargo.toml` metadata.
- Relocatable installs, allowing, for example, staging an install that
  is supposed to end up in `/usr/local` in some arbitrary directory
  writable by an ordinary user. The relocation directory is given by
  the `--dest-dir` installation option.

A consequence of the relocation feature is that it is straightforward
for `cargo-parcel` to also provide the `bundle` sub-command to
generate simple binary archives, suitable for deployment onto machines
without a Rust toolchain.

The behavior of the `cargo-parcel` subcommands are driven by the table
`package.metadata.parcel` defined in `Cargo.toml`. The contents of
this table are referred to as *parcel metadata* below.

## Installation

The `cargo parcel install` command can be though of an extended
version of `cargo install --path .` with modified defaults. It differs
from `cargo install --path` in the following ways:

- The default installation location is `~/.local`, so binaries will
  end up in `~/.local/bin` by default.
- The installation prefix can be chosen with the `--prefix` option.
- To enable a two-stage installation, where the second step only
  involves putting files into place, the `--dest-dir` option can be
  used to relocate the installation. A two-stage installation allows
  carrying out the second step as another user, or on another
  machine. For deploying to another machine, see the `bundle` command,
  which bundles the result of a relocated install into an archive
  file. The `bundle` command is described the "Distribution" section.
- Only the binaries declared in the `cargo-binaries` in the parcel
  metadata will be installed.
- The binaries will be stripped by default, unless the `--no-strip`
  option is given. This requires the `strip` command to be available,
  as e.g. provided by GNU binutils.
- When the `man-pages` key is given in the metadata, the listed man
  pages will be installed. If the man pages are supplied as Markdown
  source, [`pandoc`] is required to render them before installation.
- Additional files, given by the `pkg-data` metadata key, may be
  installed into `<PREFIX>/share/<NAME>`, where `PREFIX` is the chosen
  installation prefix, and `NAME` the crate's name.
- Using the `systemd-units` key, `systemd` unit files may be included
  in the installation.

For example, do a standard installation using the defaults:

```sh
cargo parcel install
```

Do a staged installation which should end up in `/usr/local`
eventually:

```sh
cargo parcel install --dest-dir /tmp/parcel-stage --prefix /usr/local
```

For a concise summary of the available options, use `cargo parcel help
install`.

## Uninstalling

Using `cargo parcel uninstall` you can counteract the effects of a
previous `cargo parcel install` with the same `--prefix` and
`--dest-dir` options.

Basic usage:

```sh
cargo parcel uninstall
```

For a concise summary of the available options, use `cargo parcel help
uninstall`.

## Distribution

The `cargo parcel bundle` command combines a staged installation, as
per `install`, with the creation of an archive. This creates a single
binary artifact, ready to be archived, published, or deployed to
another machine.

For a concise summary of the available options, use `cargo parcel help
bundle`.

This command currently requires GNU tar to be installed. 

Create an archive file, using the a musl-based target to get
statically-linked binaries:

```sh
cargo parcel bundle --target x86_64-unknown-linux-musl -o my-parcel.tar.gz
```

Note that if your bundle contains resource files which are referenced
via the compile-time [`PARCEL_INSTALL_PREFIX`
setting](integration.md#using-the-installation-prefix), you should
make sure the bundle gets installed at the compiled-in prefix. The
default paths of the files contained in the bundle mesh well with use
of [GNU stow]; see [below](#using-gnu-stow) for details.

The file names in the archive will all be prefixed with a combination
of the package name and the version, and not include the prefix. So
a simple bundle may contain these files:

```
./hello-world-0.0.1/share/man/man1/hello-world.1
./hello-world-0.0.1/bin/hello-world
```

### Using GNU stow

On the target machine, you can optionally use [GNU stow] to manage
multiple versions of the bundle, making uninstalling and rollback to a
previous version a simple affair, even if `cargo-parcel` is not
available on the target machine.

#### Initial installation

```sh
# Change directory to the  default prefix; relevant if the code refers
# to the compile-time `PARCEL_INSTALL_PREFIX` setting.
cd ~/.local
mkdir -p stow
cd stow
tar -xf /tmp/hello-world-0.0.1.tar.gz
stow hello-world-0.0.1
```

#### Upgrade

```sh
cd ~/.local/stow
tar -xf /tmp/hello-world-0.0.2.tar.gz
stow -D hello-world-0.0.1 -S hello-world-0.0.2
```

### Rollback

```sh
cd ~/.local/stow
stow -D hello-world-0.0.2 -S hello-world-0.0.1
```

[`pandoc`]: https://pandoc.org/
[GNU stow]: https://www.gnu.org/software/stow/
