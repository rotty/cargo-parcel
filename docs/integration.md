# Integrating `parcel-tasks` into a crate

Note the integration described in this section is optional, you can
instead just opt to require users of your crate to install the
`cargo-parcel` cargo plugin, and have them use that directly, instead
of integrating `parcel-tasks` into your crate as an `xtask` cargo
alias. By avoiding the integration, you not only make the process a
bit more involved for your users, you also expose them to potential
mismatches between the `parcel` metadata in `Cargo.toml` and the
version of `cargo-parcel` the user has installed.

Short instructions are provided in the [template
README](../templates/README.md). There is also a cargo plugin
subcommand, which automates the process for you, dropping the required
files in place:

```
$ cargo install cargo-parcel
$ cargo parcel init PARCEL-NAME
$ edit Cargo.toml # Add "xtask" to workspace members
```

For details, read on.

You can have a look at the
[`templates/hello-world`](../templates/hello-world/) directory for a
simple example project with the `cargo-parcel` boilerplate applied. It
essentially boils down to these steps:

- Create a new crate in your project root. In alignment with the
  `xtask` pattern, using the `xtask` directory below your crate root
  is suggested, but any subdirectory, also a nested one, should work
  fine.

  ```sh
  cargo new xtask
  ```
- Make the newly created crate implement a `xtask` cargo subcommand,
  by creating `.cargo/config.toml` below your crate root, with the
  following contents:

  ```toml
  [alias]
  xtask = "run --manifest-path ./xtask/Cargo.toml --"
  ```

  You can also opt to choose another name for the alias instead of
  `xtask`, but be sure not to shadow any existing cargo subcommand;
  see this [cargo
  issue](https://github.com/rust-lang/cargo/issues/10049). Originally,
  this documentation suggested using `parcel` as the subcommand name,
  but that conflicts with the `cargo parcel` subcommand available via
  installing the `cargo-parcel` crate, as per the aforementioned
  issue.

  You want to add `.cargo/config.toml` to version control, so that
  users can access the new subcommand as well.

- Adjust the new crate's `Cargo.toml` to match the following:

  ```toml
  [package]
  name = "xtask"
  version = "0.0.1"
  edition = "2021"
  publish = false

  [dependencies]
  parcel-tasks = "0.0.5"
  ```

- Replace `src/main.rs` in the new crate with this boilerplate,
  adjusting to use your project and binary name:

  ```rust
  use parcel_tasks::Parcel;

  fn main() {
      let parcel = Parcel::build("hello-world", env!("CARGO_PKG_VERSION"))
          .cargo_binaries(&["hello-world"])
          .finish();
      parcel_tasks::main();
  }
  ```

  See the [API
  docs](https://docs.rs/parcel-tasks/latest/parcel_tasks/struct.ParcelBuilder.html)
  for further configuration available on the builder.

- Include the `xtask` crate in your workspace; this will allow sharing
  the output directory between `xtask` and your crate(s), and include
  `xtask` dependency information in `Cargo.lock`. Add the `xtask`
  directory to your `workspace.members`, if you are already using a
  workspace, or add the following to `Cargo.toml`, if you don't:

  ```toml
  [workspace]
  members = ["xtask"]
  ```

Now you can use `cargo xtask install` as an extended substitute for
`cargo install`, or any of the other tasks provided by
`parcel-tasks`. Here is some example output from running the included
`hello-world` example:

```
~/src/hello-world$ cargo xtask install --dest-dir /tmp/foo --verbose
    Finished dev [unoptimized] target(s) in 0.01s
     Running `parcel/target/debug/parcel install --dest-dir /tmp/foo --verbose`
Pre-install: running cargo build (default), passing prefix /home/user/.local
* cargo build --release
   Compiling hello-world v0.0.1 (/home/user/src/crates/cargo-parcel/templates/hello-world)
    Finished release [optimized] target(s) in 0.30s
Pre-install: stripping binaries
* strip target/parcel/stripped/hello-world
Pre-install: running pandoc for man page hello-world.1.md
* pandoc -s -M section=1 -t man -o target/parcel/man/hello-world.1 hello-world.1.md
/tmp/foo/home/user/.local/bin/hello-world copied from target/parcel/stripped/hello-world (stripped cargo binary)
/tmp/foo/home/user/.local/share/man/man1/hello-world.1 copied from target/parcel/man/hello-world.1 (man page, rendered from hello-world.1.md)
```

# Using the installation prefix

The above instructions are sufficient for application crates that do
not need the installation prefix compiled into them. However, if the
installed Rust binaries need to access some resources installed along
with them, such as external translation files, having the expected
installation location compiled into the binary is required, or at
least desirable in most cases, as it spares users to specify that
location themselves.

To make use of this feature in your code, just refer to the
`PARCEL_INSTALL_PREFIX` environment variable using the `env` or
`option_env` macros from the Rust standard library. If you chose
`env` however, that will require `PARCEL_INSTALL_PREFIX` to be defined
when building the project; when calling cargo-parcel commands, such as
`cargo parcel install`, this will be taken care of, but it will break
regular `cargo build`, unless `PARCEL_INSTALL_PREFIX` is set
manually. Hence it is recommended to use `option_env!` and to provide
fallback behavior in case the environment variable is not
defined.

Take a look at
[`templates/hello-world-i18n`](../templates/hello-world-i18n/) for an
example.
