# cargo-parcel metadata reference

A simple application crate's `Cargo.toml` may look like this:

```toml
[package]
name = "hello-world"
version = "0.0.1"
authors = ["Alice Hacker <alice@example.org>"]
edition = "2021"

[dependencies]

[package.metadata.parcel]
cargo-binaries = ["hello-world"]
man-pages = ["hello-world.1.md"]
pkg-data = ["data/*"]
```

This will install the `hello-world` binary in *prefix*`/bin`, and
the result of rendering `hello-world.1.md` with `pandoc` to
*prefix*`/share/man/man1/hello-world.1`. All files and directories
under `data` will be installed to *prefix*`/share/hello-world/`.

## Workspaces

Currently, `cargo-parcel` can take its configuration from either a
cargo package manifest, or a virtual manifest (i.e., a manifest
containing a `workspace`, but no root `package`). When using a virtual
manifest, the `workspace.metadata.parcel` table needs to contain
`name` and `version` fields, which are otherwise optional and taken
from the `package` table of the non-virtual manifest.

## Settings

Inside the `package.metadata.parcel` table (or
`workspace.metadata.parcel`), the following keys can be used to
describe the parcel's contents:

- `name`

   A string. Can be used to override the crate name. This is helpful
   for crates named *foo*-cli or similar. Such crates names are a
   common occurrence, as the "actual" crate name, *foo* will be
   preferably given to the accompanying library crate, if one exists.

   Required when using a virtual manifest.

- `version`

   A string containing the version number, optionally overriding the
   `package.version`. For virtual manifests, this key is required.

- `cargo-binaries`

  A list of filenames, referring to the filenames of binaries output
  by `cargo build`. Note that *only* these binaries will be included,
  so if you do not specify this, `cargo parcel install` will not
  install any binaries, which is probably not what you want.

- `man-pages`

  A list of manual page files, either verbatim or as Markdown
  source. The file names must start conform to the pattern
  "*name*.*section*" or "*name*.*section*.md", where *name* may be
  arbitrary, but *section* must start with a number from 1 up to and
  including 8, which will be used to place them in the proper
  directory upon installation.

  When a Markdown source is used, [`pandoc`] is used to render it to
  [troff] format before installation.

- `pkg-data`

  A list of glob patterns. The files and directories matched by the
  patterns will be installed into a directory with a package-specific
  name. Note that only the last component of the matched files is used
  as at the destination name.

  The destination directory is currently
  *`prefix`*`/share/`*`pkg-name`* on all platforms, but that will
  probably change for Windows, should `cargo-parcel` ever be adapted
  to support its conventions.

- `systemd-units`

   A list of glob patterns. The files matched by the patterns will be
   installed into *`prefix`*`/lib/systemd/system`, after applying
   [substitutions].

# Substitutions

When installing, or creating an installation bundle, any text files
installed (currently only systemd unit files) are run through a
substitution process, allowing to refer to the eventual installation
location, as given specified using the `--prefix` option. For any
variable names, as defined below, the string `@variable-name@` will be
substituted with the value of the variable. So for example, `@prefix@`
will be resolved to `~/.local`, assuming the default installation
prefix. A literal "at" symbol (`@`) will be produced by a double "at"
(`@@`) regardless of defined variables.

Defined variables:

- `prefix`: The installation prefix, as supplied using the `--prefix`
  option.

[`pandoc`]: https://pandoc.org/
[troff]: https://en.wikipedia.org/wiki/Troff
